'use strict';
const nodemailer = require('nodemailer');
const bunyan = require('bunyan');
const Mailgen = require('mailgen');

/**
 * Envoie un mail de confirmation de création d'utilisateur
 */
module.exports.sendMailCreate = (request,response) => {
    console.log('Message received');

    let user = request.payload.user;

    let mailGenerator = prepareMail();

    let emailTplCreateUser = {
        body: {
            greeting: 'Bonjour',
            name: user.firstName + ' ' + user.lastName,
            intro: ['Bienvenue sur l\'application Appli Node JS !',
                'Voici vos informations de login :',
                'Login: ' + user.login,
                'Mot de passe: ' + user.password],
            action: {
                instructions: 'Pour consulter tout ça :',
                button: {
                    color: '#22BC66',
                    text: 'Accéder à l\'appli',
                    link: 'http://localhost:8080/documentation'
                }
            },
            outro: '',
            signature: 'A bientôt !'
        }
    };
    let emailBody = mailGenerator.generate(emailTplCreateUser);

    let message = {
        to: user.firstName + ' ' + user.lastName + ' <' + user.email + '>',
        subject: 'Bienvenue sur l\'application Node JS',
        html: emailBody,
    };

    sendMail(request,message);
};

/**
 * Envoie un mail d'information de changement d'utilisateur
 */
module.exports.sendMailUpdate = (request,response) => {
    console.log('Message received');

    let user = request.payload.user;

    let mailGenerator = prepareMail();

    let emailTplUpdateUser = {
        body: {
            greeting: 'Bonjour',
            name: user.firstName + ' ' + user.lastName,
            intro: ['Vos informations de login ont bien été modifiées !'],
            action: {
                instructions: 'Pour consulter tout ça :',
                button: {
                    color: '#BC890E',
                    text: 'Accéder à l\'appli',
                    link: 'http://localhost:8080/documentation'
                }
            },
            outro: '',
            signature: 'A bientôt !'
        }
    };
    let emailBody = mailGenerator.generate(emailTplUpdateUser);

    let message = {
        to: user.firstName + ' ' + user.lastName + ' <' + user.email + '>',
        subject: 'Vos informations ont été modifiées',
        html: emailBody,
    };

    sendMail(request,message);
};

/**
 * Envoie un mail de récupération de mot de passe
 */
module.exports.sendMailRecover = (request,response) => {
    console.log('Message received');

    let user = request.payload.user;
    let password = request.payload.password;

    let mailGenerator = prepareMail();

    let emailTplRecoverPassword = {
        body: {
            greeting: 'Bonjour',
            name: user.firstName + ' ' + user.lastName,
            intro: ['Une procédure de récupération de mot de passe s\'est déclenchée.',
                'Voici votre nouveau mot de passe : ' + password],
            action: {
                instructions: 'Pour consulter tout ça :',
                button: {
                    color: '#BC890E',
                    text: 'Accéder à l\'appli',
                    link: 'http://localhost:8080/documentation'
                }
            },
            outro: '',
            signature: 'A bientôt !'
        }
    };
    let emailBody = mailGenerator.generate(emailTplRecoverPassword);

    let message = {
        to: user.firstName + ' ' + user.lastName + ' <' + user.email + '>',
        subject: 'Récupération de mot de passe',
        html: emailBody,
    };

    sendMail(request,message);
};

/**
 * Génération du corps d'un mail avec Mailgen
 */
function prepareMail() {
    return new Mailgen({
        theme: 'default',
        product: {
            // Appears in header & footer of e-mails
            name: 'Appli Node JS',
            link: 'http://localhost:8080/documentation'
        }
    });
}

/**
 * Paramétrage du transporteur et envoi d'un mail
 * @param message le message à envoyer
 */
function sendMail(request,message) {
    let transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'superrestaurantbundle@gmail.com',
            pass:  'thisissparta'
        },
        logger: bunyan.createLogger({
            name: 'nodemailer'
        }),
        tls:{
            rejectUnauthorized: false
        },
        debug: false // include SMTP traffic in the logs
    }, {
        from: 'Jojo <superrestaurantbundle@gmail.com>'
    });

    transporter.sendMail(message, (error, info) => {
        if (error) {
            console.log('Error occurred');
            console.log(error.message);
            return;
        }
        console.log('Message sent successfully!');
        console.log('Server responded with "%s"', info.response);
        transporter.close();

        let io = request.plugins['hapi-io'].io;
        if (io) {
            // Message de confirmation d'envoi du mail
            io.sockets.emit('ok-mail');
            console.log('A mail has been sent to ' + user.email);
        }
    });
}