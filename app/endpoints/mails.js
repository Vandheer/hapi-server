'use strict';
const handler = require('../handlers/mails');

exports.register = (server, options, next) => {
    server.route([
        {
            method: 'POST',
            path: '/mail/create',
            config: {
                plugins: {'hapi-io': 'mail-create'}
            },
            handler: handler.sendMailCreate,
        },
        {
            method: 'POST',
            path: '/mail/update',
            config: {
                plugins: {'hapi-io': 'mail-update'}
            },
            handler: handler.sendMailUpdate,
        },{
            method: 'POST',
            path: '/mail/recover',
            config: {
                plugins: {'hapi-io': 'mail-recover'}
            },
            handler: handler.sendMailRecover,
        }
    ]);
    next();
};

exports.register.attributes = {
    name : 'mails-routes'
};
